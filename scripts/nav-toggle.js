const getTag = (tag) => document.querySelector("[data-script=" + tag + "]");
const nav_primary = getTag("nav_primary");
const nav_bgGradient = getTag("nav_bgGradient");
const nav_toggle = getTag("nav_toggle");

nav_toggle.addEventListener("click", _=> {
   // before toggling, change the aria-expanded attribute
   nav_primary.hasAttribute("data-query-visible")
      ? nav_toggle.setAttribute("aria-expanded", false)
      : nav_toggle.setAttribute("aria-expanded", true);
   
   nav_primary.toggleAttribute("data-query-visible");
   nav_bgGradient.toggleAttribute("data-query-visible");
});
