module.exports = {
   syntax: 'postcss-scss',
   plugins: [
      require('@csstools/postcss-sass'),
      require('postcss-preset-env')({stage: 1}), // experimental css features
      require('@fullhuman/postcss-purgecss')({ // removes unused css
         content: ['./**/*.html', './**/*.js'],
         css: ['./dist/style.css'],
         defaultExtractor: (content) => content.match(/[\w-/:]+(?<!:)/g) || [],
         FontFace: true,
      }),
      require('cssnano')({ // removes duplicates and shortens css
         preset: ['cssnano-preset-default', {
            normalizeWhitespace: true,
         }]
      }),
   ]
}
